<%
    if (session.getAttribute("user") != null) {
        response.sendRedirect("/DAG/index.jsp");
    }
%>
<!DOCTYPE html>
<html lang="zxx">
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <jsp:include page="stylelib.jsp"></jsp:include>
            <title>Gutim | Đăng nhập</title>
        </head>
        <body>
        <jsp:include page="header.jsp"></jsp:include>
            <!-- Breadcrumb Section Begin -->
            <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb/classes-breadcrumb.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcrumb-text">
                                <h2>Đăng nhập</h2>
                                <div class="breadcrumb-option">
                                    <a href="./index.jsp"><i class="fa fa-home"></i> Home</a>
                                    <span>Đăng nhập</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Section End -->
            <!-- Register Section Begin -->
            <section class="register-section classes-page spad">
                <div class="container">
                    <div class="classes-page-text">
                        <div class="row">

                            <div class="col-lg">
                                <div class="register-text">
                                    <div class="section-title">
                                        <h2>Đăng nhập</h2>
                                    </div>
                                    <form action="UserServlet" method="POST" class="register-form">
                                    <% if (request.getAttribute("error") != null) {%>                                  
                                    <span style="color:red"><%=request.getAttribute("error")%></span>
                                    <% }%>
                                    <div class="row">
                                        <div class="col-lg">
                                            <label for="name">Tài khoản</label>
                                            <input type="text" name="name" id="name">
                                        </div>
                                        <div class="col-lg">
                                            <label for="pass">Mật khẩu</label>
                                            <input type="password" name="pass" id="pass">
                                        </div>
                                    </div>
                                    <button type="submit" class="register-btn" value="login" name="command">Đăng nhập</button>
                                    <br><br>
                                    <a> Bạn chưa có tài khoản ?</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="./register.jsp" class="btn btn-info" role="button">Đăng ký</a>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <br><br><br>
        <!-- Register Section End -->
        <jsp:include page="footer.jsp"></jsp:include>
    </body>

</html>