<%
    if (session.getAttribute("user") != null) {
        response.sendRedirect("/DAG/index.jsp");
    }
%>
<!DOCTYPE html>
<html lang="zxx">
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <head>
        <jsp:include page="stylelib.jsp"></jsp:include>
            <title>Gutim | Đăng ký</title>
        </head>

        <body>
        <jsp:include page="header.jsp"></jsp:include>
            <!-- Breadcrumb Section Begin -->
            <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb/classes-breadcrumb.jpg">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcrumb-text">
                                <h2>Đăng ký</h2>
                                <div class="breadcrumb-option">
                                    <a href="./index.jsp"><i class="fa fa-home"></i> Home</a>
                                    <span>Đăng ký</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Section End -->

            <!-- Register Section Begin -->
            <section class="register-section classes-page spad">
                <div class="container">
                    <div class="classes-page-text">
                        <div class="row">
                            <div class="col-lg">
                                <div class="register-text">
                                    <div class="section-title">
                                        <h2>Đăng ký</h2>
                                        <p>Đăng ký ngay để nhận thêm nhiều ưu đãi</p>
                                    </div>
                                    <form action="UserServlet" method="POST" class="register-form">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="name">Tên đăng nhập</label>
                                                <input type="text" name="name" id="name">
                                                <label id="user-result"></label>
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" id="email">
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="phone">Số điện thoại</label>
                                                <input type="text" name="phone" id="phone">
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="pass">Mật khẩu</label>
                                                <input type="password" name="pass" id="pass">
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="confirm_pass">Xác nhận mật khẩu</label>
                                                <input type="password" name="confirm_pass" id="confirm_pass">
                                                <label id='message'></label>
                                            </div>
                                        </div>
                                        <button type="submit" value="insert" name="command" class="register-btn">Đăng ký</button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <br><br><br>
            <!-- Register Section End -->
        <jsp:include page="footer.jsp"></jsp:include>
        <script type="text/javascript">
            $(document).ready(function () {
                var x_timer;
                $("#name").keyup(function (e) {
                    clearTimeout(x_timer);
                    var user_name = $(this).val();
                    x_timer = setTimeout(function () {
                        check_username_ajax(user_name);
                    }, 1000);
                });
                function check_username_ajax(username) {
                    $("#user-result").html('<img src="img/ajax-loader.gif" />');
                    $.post('CheckEmailServlet', {'username': username}, function (data) {
                        $("#user-result").html(data);
                    });
                }
            });
            function trim(str) {
                var str = str.replace(/^\s+|\s+$/g, "");
                return str;
            }
            function ValidCaptcha(a, b) {
                var a = trim(a);
                if (a == b) {
                    return true;
                }
                return false;
            }
            function checkform(theform, b) {
                var why = "";
                var a = theform.input.value;
                if (a == "") {
                    why += "Security code should not be empty.\n";
                }
                if (a != "") {
                    if (!ValidCaptcha(a, b)) {
                        why += "Security code did not match.\n";
                    }
                }
                if (why != "") {
                    alert(why);
                    return false;
                }
            }
            var a = Math.ceil(Math.random() * 9);
            var b = Math.ceil(Math.random() * 9);
            var code = a + b;

        $('#pass, #confirm_pass').on('keyup', function () {
                if ($('#pass').val() == $('#confirm_pass').val()) {
                    $('#message').html('Trùng khớp').css('color', 'green');
                } else
                    $('#message').html('Không trùng khớp').css('color', 'red');
            });

        </script>
    </body>

</html>