<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="get.CourseGet"%>
<%@page import="model.Course"%>
<%@page import="java.util.Map"%>
<%@page import="model.Item"%>
<%@page import="model.Cart"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <jsp:include page="stylelib.jsp"></jsp:include>
            <title>Gutim | Khóa học</title>
        <body>
        <jsp:include page="header.jsp"></jsp:include>
        <%
            Cart cart = (Cart) session.getAttribute("cart");
            if (cart == null) {
                cart = new Cart();
                session.setAttribute("cart", cart);
            }
            DecimalFormat formatter = new DecimalFormat("###,###,###");
        %>
        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb/classes-breadcrumb.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-text">
                            <h2>Giỏ hàng</h2>
                            <div class="breadcrumb-option">
                                <a href="./index.jsp"><i class="fa fa-home"></i> Home</a>
                                <span>Giỏ hàng</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Blog Section Begin -->
        <section class="blog-section spad">
            <div class="empty-cart">
                Giỏ hàng trống
            </div>
            <div class="check-out">	 
                <div class="container">	 
                    <table class="table animated wow fadeInLeft" data-wow-delay=".5s">
                        <tr>
                            <th class="t-head">Hình ảnh</th>
                            <th class="t-head ">Mặt Hàng</th>
                            <th class="t-head">Giá</th>
                            <th class="t-head">Số Lượng</th>
                            <th class="t-head"></th>

                        </tr>
                        <%for (Map.Entry<Long, Item> list : cart.getCartItems().entrySet()) {%>
                        <tr class="cross1">
                            <td class="t-data">
                                <a href="./course-details.jsp?course=<%=list.getValue().getCourse().getCourseID()%>" class="at-in">
                                    <img src="<%=list.getValue().getCourse().getCourseImg()%>" class="fashion-grid" alt=""> 

                                </a>
                            </td>
                            <td class="t-data">

                                <h3><%=list.getValue().getCourse().getCourseName()%></h3>
                            </td>
                            <td class="t-data"><%=formatter.format(list.getValue().getCourse().getCoursePrice())%> VNĐ</td>
                            <td class="t-data">
                                <div class="quantity"> 
                                    <div class="quantity-select">            
                                        <a href="CartServlet?command=sub&course_id=<%=list.getValue().getCourse().getCourseID()%>"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                        <div><span class="span-1"><%=list.getValue().getQuantity()%></span></div>									
                                        <a href="CartServlet?command=plus&course_id=<%=list.getValue().getCourse().getCourseID()%>"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                            <td class="t-data">
                                <a href="CartServlet?command=remove&course_id=<%=list.getValue().getCourse().getCourseID()%>"><img src="img/circle.png" class="img-responsive " alt=""></a>
                            </td>
                        </tr>
                        <% }%>

                    </table>
                    <div class=" cart-total">
                        <h5 class="continue" >Tổng Cộng Giỏ Hàng</h5>
                        <div class="price-details">
                            <h3>Chi Tiết Giá</h3>
                            <span>Tổng Cộng</span>
                            <span class="total1"></span>
                            <span>Giảm giá</span>
                            <span class="total1">---</span>
                            <span>Phí vận chuyển</span>
                            <span class="total1"></span>
                            <div class="clearfix"></div>				 
                        </div>	
                        <ul class="total_price">
                            <li> <h4>Tổng Cộng</h4></li>	
                            <li class="last_price"><span><%=formatter.format(cart.totalCart())%> VNĐ</span></li>
                            <div class="clearfix"> </div>
                        </ul>
                        <!--<a href="checkoutstep.jsp">Thanh Toán</a>-->
                        <button onclick="window.location.href = 'checkoutstep.jsp'">Thanh Toán</button>
                    </div>

                </div>
            </div>
        </section>
        <!--quantity-->
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <script>
                            var a = <%=(formatter.format(cart.countItem()))%>;
                            if (a == 0) {
                                $('.check-out').hide();
                            } else {
                                $('.empty-cart').hide();
                            }

        </script>
        <!--quantity-->
        <!-- Blog Section End -->
        <jsp:include page="footer.jsp"></jsp:include>
    </body>

</html>