<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!-- Footer Banner Section Begin -->
    <section class="footer-banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="footer-banner-item set-bg" data-setbg="img/footer-banner/footer-banner-1.jpg">
                        <span>Thành viên mới</span>
                        <h2>7 ngày miễn phí</h2>
                        <p>Hoàn thành các buổi đào tạo với chúng tôi,chắc chắn bạn sẽ rất hài lòng</p>
                        <a href="./register.jsp" class="primary-btn">Đăng ký ngay</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer-banner-item set-bg" data-setbg="img/footer-banner/footer-banner-2.jpg">
                        <span>Liên hệ với chúng tôi</span>
                        <h2>+84 28 3526 8799</h2>
                        <p>Chúng tôi sẽ đưa ra giải pháp cho một kết hoạch tập luyện hoàn hảo dành cho bạn !</p>
                        <a href="./register.jsp" class="primary-btn">Đăng ký ngay</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Footer Banner Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="contact-option">
                        <span>Số điện thoại</span>
                        <p>+84 28 3526 8799</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-option">
                        <span>Địa chỉ</span>
                        <p>391A Nam Kỳ Khởi Nghĩa, Phường 14, Quận 3, Hồ Chí Minh</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="contact-option">
                        <span>Email</span>
                        <p>contactcompany@Gutim.com</p>
                    </div>
                </div>
            </div>
            <div class="subscribe-option set-bg" data-setbg="img/footer-signup.jpg">
                <div class="so-text">
                    <h4>Nhận thông tin mới từ chúng tôi</h4>
                    <p>Đăng ký để nhận thông tin mới nhất </p>
                </div>
                <form action="#" class="subscribe-form">
                    <input type="text" placeholder="Email của bạn">
                    <button type="submit"><i class="fa fa-send"></i></button>
                </form>
            </div>
            <div class="copyright-text">
                <ul>
                    <li><a href="#">Term&Use</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
                <p>&copy;<p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</p>
                <div class="footer-social">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-dribbble"></i></a>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <!--<script src="js/bootstrap.min.js"></script>-->
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>