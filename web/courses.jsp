<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Course"%>
<%@page import="get.CourseGet"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <jsp:include page="stylelib.jsp"></jsp:include>
            <title>Gutim | Khóa học</title>
        <body>
        <jsp:include page="header.jsp"></jsp:include>
        <%
            CourseGet courseGet = new CourseGet();
            String category_id_1 = "";
            String category_id_2 = "";
            if (request.getParameter("category") != null) {
                category_id_1 = request.getParameter("category");
                category_id_2 = request.getParameter("category");
            }
        %>
        <!-- Breadcrumb Section Begin -->
        <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb/classes-breadcrumb.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-text">
                            <h2>Khóa học</h2>
                            <div class="breadcrumb-option">
                                <a href="./index.jsp"><i class="fa fa-home"></i> Home</a>
                                <span>Yoga</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Breadcrumb Section End -->

        <!-- Blog Section Begin -->
        <section class="blog-section spad">
            <div class="container">
                <div class="row">
                    <%
                        for (Course course : courseGet.getListCourseByCategory(Long.parseLong(category_id_1))) {
                    %>
                    <div class="col-lg-4 col-md-6">
                        <div class="single-blog-item">
                            <img src="<%= course.getCourseImg()%>" alt="">
                            <div class="blog-widget">
                                <div class="bw-date">February 17, 2019</div>
                                <a href="#" class="tag">#Yoga</a>
                            </div>
                            <h4><a href="./course-details.jsp?course=<%= course.getCourseID()%>"><%= course.getCourseName()%></a></h4>
                            <h4><a href="CartServlet?command=plus&course_id=<%= course.getCourseID()%>">Thêm vào giỏ hàng</a></h4>
                        </div>
                    </div>
                    <% }%>

                </div>
            </div>
        </section>
        
        <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
        <!-- Blog Section End -->
        <jsp:include page="footer.jsp"></jsp:include>
    </body>

</html>