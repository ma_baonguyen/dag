<%@page import="java.util.Map"%>
<%@page import="model.User"%>
<%@page import="get.UserGet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%
    User user = null;
    if (session.getAttribute("user") != null) {
        user = (User) session.getAttribute("user");
    }else{
        response.sendRedirect("/DAG/login.jsp");
    }
     UserGet userGet = new UserGet();
%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <jsp:include page="stylelib.jsp"></jsp:include>
            <title>Gutim | Tài khoản của tôi</title>
        </head>
        <body>
        <jsp:include page="header.jsp"></jsp:include>
            <!-- Breadcrumb Section Begin -->
            <section class="breadcrumb-section set-bg" data-setbg="img/breadcrumb/classes-breadcrumb.jpg" style="height: 0%;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="breadcrumb-text">
                                <h2>Thông tin cá nhân</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Breadcrumb Section End -->
            <!-- Register Section Begin -->
            <section class="register-section classes-page spad">
                <div class="container">
                    <div class="classes-page-text">
                        <div class="row">

                            <%if (user != null) {%>
                            <a><%=userGet.getUser(user.getUserID()).getUserName()%></a>
                            <br>
                            <a><%=userGet.getUser(user.getUserID()).getUserEmail()%></a>
                            <br>
                            <a><%=userGet.getUser(user.getUserID()).getUserPhone()%></a>
                            <% } %>

                    </div>
                </div>
            </div>
        </section>
        <br><br><br>
        <!-- Register Section End -->
        <jsp:include page="footer.jsp"></jsp:include>
    </body>

</html>