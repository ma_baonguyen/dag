<%@page import="java.text.DecimalFormat"%>
<%@page import="model.Cart"%>
<%@page import="java.util.Map"%>
<%@page import="model.User"%>
<%@page import="model.Category"%>
<%@page import="get.CategoryGet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%
    User user = null;
    if (session.getAttribute("user") != null) {
        user = (User) session.getAttribute("user");
    }
    CategoryGet categoryget = new CategoryGet();
    Cart cart = (Cart) session.getAttribute("cart");
    if (cart == null) {
        cart = new Cart();
        session.setAttribute("cart", cart);
    }
    DecimalFormat formatter = new DecimalFormat("###,###,###");
%>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header Section Begin -->
<header class="header-section">
    <div class="container">
        <div class="logo">
            <a href="./index.jsp">
                <img src="img/logo.png" alt="">
            </a>
        </div>
        <div class="nav-menu">
            <nav class="mainmenu mobile-menu">
                <ul>
                    <li><a href="./index.jsp">Trang chủ</a></li>
                    <li><a href="./newmem.jsp">Dành cho người mới</a></li>
                    <li><a href="./trainer.jsp">Huấn luyện viên</a></li>
                    <li class="dropdown"><a href="#">Khóa Học</a>
                        <ul>
                            <%
                                for (Category c : categoryget.getListCategory()) {
                            %>
                            <li><a href="courses.jsp?category=<%=c.getCategoryID()%>"><%=c.getCategoryName()%></a></li>
                                <%
                                    }
                                %>
                        </ul>
                    </li>
                    <li><a href="./contact.jsp">Liên hệ</a></li>
                    <li><a href="checkout.jsp"><i class="fa fa-shopping-cart" aria-hidden="true" style="color: white;"></i>&nbsp;&nbsp;<%=formatter.format(cart.countItem())%></a></li>
                </ul>
            </nav>
            <%if (user != null) {%>
            <div class="dropdown">
                <a href="#" class="primary-btn signup-btn dropbtn"><%=user.getUserName()%></a>
                <div class="dropdown-content">
                    <a href="./profile.jsp">Tài khoản của tôi</a>
                    <a href="/DAG/LogoutServlet">Đăng xuất</a>
                </div>
            </div>
            <% } %>
            <%if (user == null) { %>
            <a href="./login.jsp" class="primary-btn signup-btn">Tài khoản</a>
            <% }%>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>
<!-- Header End -->
<style>
    .dropdown {
        position: relative;
        display: inline-block;
    }

    .dropdown-content {
        right: 0px;
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 7rem;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {background-color: #f1f1f1}

    .dropdown:hover .dropdown-content {
        display: block;
    }

    .dropdown li {
        display: block;
        float: left;
        position: relative;
        text-decoration: none;
        transition-duration: 0.5s;
    }
    .dropdown ul li a{
        font-weight: 400 !important;
        background-color: transparent;
        color: graytext !important;
        text-decoration: none;
        display: block;
        padding: 10px !important;

    }
    .dropdown ul {
        background: white;
        visibility: hidden;
        opacity: 0;
        min-width: 7rem;
        position: absolute;
        transition: all 0.5s ease;
        margin-top: 1rem;
        left: 0;
        display: none;
    }

    ul li:hover > ul,
    ul li ul:hover {
        visibility: visible;
        opacity: 1;
        display: block;
    }

    .dropdown  ul li ul li {
        clear: both;
        width: 100%;
    }
</style>
