<%@page import="java.util.Map"%>
<%@page import="model.UserAdmin"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    UserAdmin user = null;
    if (session.getAttribute("useradmin") != null) {
        user = (UserAdmin) session.getAttribute("useradmin");
    }else{
        response.sendRedirect("/DAG/admin/login.jsp");
    }
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <jsp:include page="head-lib.jsp"></jsp:include>
            <title>SB Admin 2 - Dashboard</title>
        </head>

        <body id="page-top">
            <!-- Page Wrapper -->
            <div id="wrapper">
            <jsp:include page="header.jsp"></jsp:include>
                <!-- Content Wrapper -->
                <div id="content-wrapper" class="d-flex flex-column">
                    <!-- Main Content -->
                    <div id="content">
                    <jsp:include page="topbar.jsp"></jsp:include>
                    <jsp:include page="content.jsp"></jsp:include>
                    </div>
                    <!-- End of Main Content -->
                    <!-- Footer -->
                    <footer class="sticky-footer bg-white">
                        <div class="container my-auto">
                            <div class="copyright text-center my-auto">
                                <span>Copyright &copy; Your Website 2020</span>
                            </div>
                        </div>
                    </footer>
                    <!-- End of Footer -->
                </div>
                <!-- End of Content Wrapper -->

            </div>
            <!-- End of Page Wrapper -->
        <jsp:include page="footer.jsp"></jsp:include>
    </body>

</html>
