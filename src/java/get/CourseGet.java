/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package get;

import connect.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Course;

/**
 *
 * @author baong
 */
public class CourseGet {
    public ArrayList<Course> getListCourseByCategory(long category_id) throws SQLException {
        Connection connection = DBConnect.getConnecttion();
        String sql = "SELECT * FROM courses WHERE CategoryID = '" + category_id + "'";
        PreparedStatement ps = connection.prepareCall(sql);
        ResultSet rs = ps.executeQuery();
        ArrayList<Course> list = new ArrayList<>();
        while (rs.next()) {
            Course course = new Course();
            course.setCourseID(rs.getLong("CourseID"));
            course.setCategoryID(rs.getLong("CategoryID"));
            course.setCoachID(rs.getLong("CoachID"));
            course.setCourseName(rs.getString("CourseName"));
            course.setCourseDesc(rs.getString("CourseDesc"));
            course.setCourseImg(rs.getString("CourseImg"));
            course.setCoursePrice(rs.getLong("CoursePrice"));
            list.add(course);
        }
        return list;
    }
    
    public Course getCourseById(long course_id) throws SQLException {
     Connection connection = DBConnect.getConnecttion();
     String sql = "SELECT * FROM courses WHERE CourseID = '" + course_id + "'";
     PreparedStatement ps = connection.prepareCall(sql);
     ResultSet rs = ps.executeQuery();
     Course course = new Course();
     while (rs.next()) {
            course.setCourseID(rs.getLong("CourseID"));
            course.setCategoryID(rs.getLong("CategoryID"));
            course.setCoachID(rs.getLong("CoachID"));
            course.setCourseName(rs.getString("CourseName"));
            course.setCourseDesc(rs.getString("CourseDesc"));
            course.setCourseImg(rs.getString("CourseImg"));
            course.setCoursePrice(rs.getLong("CoursePrice"));
     }
     return course;
}
}
