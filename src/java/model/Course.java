/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author baong
 */
public class Course {
    private long courseID;
    private long categoryID;
    private long coachID;
    private String courseName;
    private String courseDesc;
    private String courseImg;
    private long coursePrice;

public Course(){
    
}
public Course(long courseID, long categoryID, long coachID, String courseName, String courseDesc, String courseImg, long coursePrice) {
        this.courseID = courseID;
        this.categoryID = categoryID;
        this.coachID = coachID;
        this.courseName = courseName;
        this.courseDesc = courseDesc;
        this.courseImg = courseImg;
        this.coursePrice = coursePrice;
    }

    public long getCourseID() {
        return courseID;
    }

    public void setCourseID(long courseID) {
        this.courseID = courseID;
    }

    public long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(long categoryID) {
        this.categoryID = categoryID;
    }

    public long getCoachID() {
        return coachID;
    }

    public void setCoachID(long coachID) {
        this.coachID = coachID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseDesc() {
        return courseDesc;
    }

    public void setCourseDesc(String courseDesc) {
        this.courseDesc = courseDesc;
    }

    public String getCourseImg() {
        return courseImg;
    }

    public void setCourseImg(String courseImg) {
        this.courseImg = courseImg;
    }

    public long getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(long coursePrice) {
        this.coursePrice = coursePrice;
    }

}
